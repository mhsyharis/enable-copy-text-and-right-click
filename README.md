**Introduction**

The "_**Enable Copy Text & Right Click**_" extension for Google Chrome is designed to empower users by allowing them to copy text and right-click on websites that have disabled these functionalities through CSS. This extension ensures that users can freely select, copy, and interact with content on all websites.

**Features**
- **Enable Copying:** Re-enables the ability to copy text from websites that have disabled text selection.
- **Enable Right Click:** Restores the right-click context menu on sites that have disabled it.
- **Easy to Use:** Simple and straightforward interface with minimal configuration needed.
- **Lightweight and Efficient:** Runs smoothly without impacting browser performance.

**Installation**

1. Download the extension package.
1. Open Google Chrome and navigate to chrome://extensions/.
1. Enable "Developer mode" at the top right corner of the page.
1. Click on "Load unpacked" and select the extracted folder of the "Enable Copy Text & Right Click" extension.
1. The extension should now appear in your extensions list and is ready to use.


**Usage**

Once installed, the extension works automatically on all websites. You just have to click on extension icon and press "YES"and you can now copy text and use the right-click context menu on the webpage that previously restricted these actions.

**Contributing**

I welcome contributions from the community, whether it's through bug reports, feature suggestions, or code contributions..

**License**

This extension is released under the MIT License. Please refer to the LICENSE file for the full text.
