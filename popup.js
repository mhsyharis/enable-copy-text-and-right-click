document.getElementById('yesBtn').addEventListener('click', function() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.scripting.executeScript({
            target: {tabId: tabs[0].id},
            function: applyUserSelectStyle
        });
    });
    window.close(); // Close the popup after the click
});

document.getElementById('noBtn').addEventListener('click', function() {
    window.close(); // Close the popup without doing anything
});


function applyUserSelectStyle() {
  

    document.addEventListener('contextmenu', event => event.stopPropagation(), { capture: true });
    var style = document.createElement('style');  
    style.textContent = `:root { user-select: auto !important; }`;
    document.head.appendChild(style);
  
}